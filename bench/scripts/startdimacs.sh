#!/bin/sh
# Script de inicializacao do condor DAG

cd /opt/bench/data
/opt/bench/bin/dimacstg 100 dimacs
/opt/bench/scripts/getpeso.sh
cd ..
/opt/bench/bin/dimacsdag >> /opt/bench/log/error_file
/opt/bench/bin/setpeso
mv /opt/bench/job.* /opt/bench/taskdata/
cp ./data/scheduling.dag ./taskdata
cd /opt/bench/taskdata/
mkdir log
ts condor_submit_dag scheduling.dag
