#!/bin/bash
qtd_tg=`ls /opt/bench/data/TASK_* | wc -w`

for (( i=0; i<$qtd_tg; i++ ))
do
  cd /opt/bench/taskdata/tg-$i
  qtd_job=`ls job.t* | wc -w`
  for (( j=0; j<$qtd_job; j++ ))
  do
    inicio=`cat scheduling-$i.dag.dagman.out | grep "Submitting Condor Node t"$i"_"$j | awk '{print $1,$2}'`
    fim=`cat scheduling-$i.dag.dagman.out | grep "Node t"$i"_"$j" job proc" | awk '{print $1,$2}'`
    echo "t"$i"_"$j "Start" $inicio >> results-$i.txt
    echo "t"$i"_"$j "End  " $fim >> results-$i.txt
  done
done