#!/bin/bash
# Author: Anderson Moreira
# mail: alsm4@cin.ufpe.br
# date: 20/10/2012
# Script for monitoring the CPU usage of a VM
# Requires sysstat package - apt-get install sysstat and sshpass - apt-get install sshpass
# %usr - Show the percentage of CPU utilization that occurred while executing at the user level (application).
# %nice - Show the percentage of CPU utilization that occurred while executing at the user level with nice priority.
# %sys - Show the percentage of CPU utilization that occurred while executing at the system level (kernel). Note that this does not include time spent servicing hardware and software interrupts.
# %iowait - Show the percentage of time that the CPU or CPUs were idle during which the system had an outstanding disk I/O request.
# %irq - Show the percentage of time spent by the CPU or CPUs to service hardware interrupts.
# %soft - Show the percentage of time spent by the CPU or CPUs to service software interrupts.
# %steal - Show the percentage of time spent in involuntary wait by the virtual CPU or CPUs while the hypervisor was servicing another virtual processor.
# %guest - Show the percentage of time spent by the CPU or CPUs to run a virtual processor.
# %idle - Show the percentage of time that the CPU or CPUs were idle and the system did not have an outstanding disk I/O request.

# Returns IP
ip=`cat /etc/network/interfaces | grep -i address | awk '{print $2}'`

# Writes the header for the identification of data
echo "%usr %sys %iowait %idle date time" > vmcpu-$ip.txt
echo "%usr %sys %iowait %idle date time"

while [ True ]
do

   # Stores only the fields of interest
   cpu=`mpstat 30 1| grep all | head -n1 | awk '{print $3,$5,$6,$11}'`

   # Obtains the current time, in the format RFC3339: AAAA-MM-DD HH:MM:SS
   # The corresponds time of mpstat return,
   # therefore the use of cpu is the average of the last minute
   tempo=`date --rfc-3339=seconds`

   # Separates date and hour of the time obtained
   data=`echo $tempo | cut -f1 -d\ `
   hora=`echo $tempo | cut -f2 -d\ | awk 'BEGIN{FS="-"}{print $1}'`

   # Show on the screen the information captured by script
   echo $cpu $data $hora

   # Writes disc information in the file
   echo $cpu $data $hora >> vmcpu-$ip.txt
   echo MARK AT `date` > cpu-$ip.mrc

   sshpass -p condor scp vmmem* condor@node2.intranet:/opt/bench/log
   sshpass -p condor scp *.mrc condor@node2.intranet:/opt/bench/log

   #Executes the script every X unit of time (seconds)
   #Commented because the mpstat already expects 30 secs
   #sleep 30

done
