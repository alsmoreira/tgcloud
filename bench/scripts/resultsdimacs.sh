#!/bin/bash

agora=`date +%d-%m-%y_%H:%M:%S`
i=0
k=42

cd /opt/bench/taskdata
mkdir /opt/bench/results/$agora

function saveresults(){
         for((i=1;i<=5;i++));
         do
           echo -ne "\e[${1}m \e[0m $((i * 5))% \b\b\b\b\b"
           sleep .01
         done
           cat /opt/bench/taskdata/scheduling.dag.dagman.log | grep submitted | awk '{print $3,$4,$6}' > /opt/bench/taskdata/time.dat
           cat /opt/bench/taskdata/scheduling.dag.dagman.log | grep terminated | awk '{print $3,$4,$6}' >> /opt/bench/taskdata/time.dat
           cd /opt/bench/taskdata/
           /opt/bench/scripts/calctime.py >> /opt/bench/taskdata/time.dat
           cp /opt/bench/taskdata/time.dat /opt/bench/results/$agora/time.dat
           i=$(($i+1))

         for((i=6;i<=10;i++));
         do
           echo -ne "\e[${1}m \e[0m $((i * 5))% \b\b\b\b\b"
           sleep .01
         done

         for((i=11;i<=15;i++));
         do
           echo -ne "\e[${1}m \e[0m $((i * 5))% \b\b\b\b\b"
           sleep .01
         done

         for((i=16;i<=19;i++));
         do
           echo -ne "\e[${1}m \e[0m $((i * 5))% \b\b\b\b\b"
           sleep .01
         done
         cp /opt/bench/log/error_file /opt/bench/results/$agora

         echo $((i * 5))
}

echo "Save DATA - DIMACS in /opt/bench/results/"$agora
saveresults $k
