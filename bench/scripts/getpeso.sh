#!/bin/bash
cat /opt/bench/data/dimacs.gr | awk '{print $1}' > /opt/bench/data/peso.dat
nodes=`head -n1 /opt/bench/data/peso.dat`
i=0

while [ $i -lt $nodes ];
do
    echo $i >> /opt/bench/data/nodes.dat
    i=$(($i+1))
done