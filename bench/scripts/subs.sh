#!/bin/bash

tg=`ls -d /opt/bench/taskdata/tg-* | wc -l`
job=`ls job.t* | wc -w`
ini=0

while [ $tg -ne $ini ]
do

  cd /opt/bench/taskdata/tg-$(($tg-1))
  #remover linha
  sed -i 3d job.t*

  #incluir linha
  sed -i '3s/^/Requirements = (Name == "intranet")\n/' job.t*

  tg=$(($tg - 1))
  
done