#!/bin/sh
# Calcula o valor do dealtime de um conjunto de tasks
# Conta a quantidade de jobs na pasta
qtd_task=`ls job.t* | wc -w`

# Cria lista de tasks
task=`ls job.t* | cut -d" " -f"$qtd_task"`

# Inicializa variáveis de armazenamento
ini=0
dealtime=0

while [ $qtd_task -ne $ini ]
do

  tmp=`echo $task | awk '{print $('$qtd_task')}'`

  arg_val=`cat $tmp | grep Arguments | awk '{print $3}'`
  dealtime=`echo "$dealtime + $arg_val" | bc`


  qtd_task=$(($qtd_task - 1))

done

# Obtem o tempo atual, no formato RFC3339: AAAA-MM-DD HH:MM:SS
tempo=`date --rfc-3339=seconds`

# Separa data e hora do tempo obtido
data=`echo $tempo | cut -d\  -f1`
hora=`echo $tempo | cut -d\  -f2| awk 'BEGIN{FS="-"}{print $1}'`

#Esscreve no arquivo as informacoes capturadas
echo $dealtime $data $hora > monitoramento.txt
mv monitoramento.txt /opt/bench/results