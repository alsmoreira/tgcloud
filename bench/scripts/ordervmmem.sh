#! /bin/sh

cat /opt/bench/log/order-* > /opt/bench/log/order.tmp
cat /opt/bench/log/order.tmp | sort -r  | awk '{print $2}' > /opt/bench/data/order.dat