#!/bin/bash
qtd_tg=`ls /opt/bench/data/TASK_* | wc -w`
qtd_tg=$(($qtd_tg-1))
agora=`date +%d-%m-%y_%H:%M:%S`
mkdir /opt/bench/taskdata/tg-$agora
mkdir /opt/bench/taskdata/tg-$agora/log

ini=-1
while [ $qtd_tg -ne $ini ]
do
  cat /opt/bench/data/TASK_GRAPH_$qtd_tg | grep 'TASK t' > /opt/bench/data/tasks-$qtd_tg.dat
  cat /opt/bench/data/TASK_GRAPH_$qtd_tg | grep 'ARC a' > /opt/bench/data/arcs-$qtd_tg.dat
  cat /opt/bench/data/tasks-$qtd_tg.dat | awk '{print $2}' > /opt/bench/data/t.dat
  cat /opt/bench/data/arcs-$qtd_tg.dat | awk '{print $4,$6}' > /opt/bench/data/parent.dat
  echo "EOF" >> /opt/bench/data/parent.dat
  #Retira a task e seu tipo
  cat /opt/bench/data/tasks-$qtd_tg.dat | awk '{print $2,$4}' > /opt/bench/data/task_type.dat
  echo "EOF" >> /opt/bench/data/task_type.dat
  #Retira o tipo e seu peso
  cat /opt/bench/data/PACKS.dat | awk '{print $1,$2}' > /opt/bench/data/type.dat

  cd /opt/bench
  echo "tg-"$qtd_tg "Start on " `date` >> /opt/bench/log/log_file
  /opt/bench/bin/startjob >> /opt/bench/log/log_file
  /opt/bench/bin/setpeso >> /opt/bench/log/log_file
  mv job.* /opt/bench/taskdata/tg-$agora
  /opt/bench/bin/startdag >> /opt/bench/log/log_file
  cp ./data/head.dag ./data/head-$qtd_tg.dag
  cp ./data/body.dag ./data/body-$qtd_tg.dag
  cp ./data/middle.dag ./data/middle-$qtd_tg.dag

  qtd_tg=$(($qtd_tg-1))

done

cat /opt/bench/data/head-* > /opt/bench/data/head.dag
cat /opt/bench/data/body-* > /opt/bench/data/body.dag
cat /opt/bench/data/middle-* > /opt/bench/data/middle.dag
cat /opt/bench/data/head.dag > /opt/bench/data/scheduling.dag
cat /opt/bench/data/middle.dag >> /opt/bench/data/scheduling.dag
cat /opt/bench/data/body.dag >> /opt/bench/data/scheduling.dag

rm -rf /opt/bench/data/head*
rm -rf /opt/bench/data/body*
rm -rf /opt/bench/data/middle*
mv /opt/bench/data/scheduling.dag /opt/bench/taskdata/tg-$agora
cd /opt/bench/taskdata/tg-$agora/
ts condor_submit_dag scheduling.dag