#!/bin/bash
# Author: Anderson Moreira
# mail: alsm4@cin.ufpe.br
# date: 20/10/2012
# Script for monitoring the use of RAM on a VM
# Using sshpass - apt-get install sshpass

# Returns IP
ip=`cat /etc/network/interfaces | grep -i address | awk '{print $2}'`

# Writes the header for the identification of data
echo "Mem_used Mem_free Mem_buffers Mem_cached Swap_used Swap_free Date Time" > vmmem-$ip.txt
echo "Mem_used Mem_free Mem_buffers Mem_cached Swap_used Swap_free Date Time"

while [ True ]
do

   # Obtains the current time, in the format RFC3339: AAAA-MM-DD HH:MM:SS
   tempo=`date --rfc-3339=seconds`
   memory=`free | grep Mem:`
   swap=`free | grep Swap:`

   # Stores only the fields of interest: Free, Used, Buffers, Cached, ...
   memfree=`echo $memory | awk '{print $4}'`
   memused=`echo $memory | awk '{print $3}'`
   membuff=`echo $memory | awk '{print $6}'`
   memcache=`echo $memory | awk '{print $7}'`

   swapfree=`echo $swap | awk '{print $4}'`
   swapused=`echo $swap | awk '{print $3}'`

   # Separates date and hour of the time obtained
   data=`echo $tempo | cut -f1 -d\ `
   hora=`echo $tempo | cut -f2 -d\ | awk 'BEGIN{FS="-"}{print $1}'`

   # Show on the screen the information captured by script
   echo $memused $memfree $membuff $memcache $swapused $swapfree $data $hora

   # Writes in the file information about RAM
   echo $memused $memfree $membuff $memcache $swapused $swapfree $data $hora >> vmmem-$ip.txt
   echo MARK AT `date` > mem-$ip.mrc
   echo $memfree `hostname` > order-$ip.dat

   sshpass -p condor scp vmmem* condor@node2.intranet:/opt/bench/log
   sshpass -p condor scp *.mrc condor@node2.intranet:/opt/bench/log
   sshpass -p condor scp order* condor@node2.intranet:/opt/bench/log

   # Executes the script every X unit of time (seconds)
   sleep 30

done

