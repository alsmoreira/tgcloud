#!/bin/sh
# Script de inicializacao do condor DAG

cd /opt/bench/data
echo "Start PACK" on `date` > /opt/bench/log/log_file
/opt/bench/bin/starttgff 4 >> /opt/bench/log/log_file
tgff vm_task
/opt/bench/bin/detachfiles >> /opt/bench/log/log_file
/opt/bench/scripts/setup.sh