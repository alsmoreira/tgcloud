#!/usr/bin/env python
# -- coding: utf-8 --

'''
	Author: Anderson Moreira
	e-mail: alsm4@cin.ufpe.br
	Converte duas string de data para formato Epoch, em segundos,
	e calcula a diferenca de tempo entre as duas datas.
'''

import time, calendar

arquivo = open("time.dat", "r")

# Formato da data: 20-11-2012-15:30:40
time_string_format = "%m/%d-%H:%M:%S"

time_string = arquivo.readline().split(" ")
time1 = time.strptime(time_string[0] + "-" + time_string[1], time_string_format)

time_string2 = arquivo.readline().split(" ")
time2 = time.strptime(time_string2[0] + "-" + time_string2[1], time_string_format)

print "Tempo de execucao", calendar.timegm(time2) - calendar.timegm(time1), "Seg"
