#!/bin/sh
mes_i=`cat time.dat | grep submitted | awk '{print $1,$2}' | cut -c 1-2`
dia_i=`cat time.dat | grep submitted | awk '{print $1,$2}' | cut -c 4-5`
hor_i=`cat time.dat | grep submitted | awk '{print $1,$2}' | cut -c 7-8`
min_i=`cat time.dat | grep submitted | awk '{print $1,$2}' | cut -c 10-11`
seg_i=`cat time.dat | grep submitted | awk '{print $1,$2}' | cut -c 13-14`

mes_f=`cat time.dat | grep terminated | awk '{print $1,$2}' | cut -c 1-2`
dia_f=`cat time.dat | grep terminated | awk '{print $1,$2}' | cut -c 4-5`
hor_f=`cat time.dat | grep terminated | awk '{print $1,$2}' | cut -c 7-8`
min_f=`cat time.dat | grep terminated | awk '{print $1,$2}' | cut -c 10-11`
seg_f=`cat time.dat | grep terminated | awk '{print $1,$2}' | cut -c 13-14`

calc1=`echo $mes_f*30*86400 - $mes_i*30*86400 | bc`
calc2=`echo $dia_f*86400 - $dia_i*86400 | bc`
calc3=`echo $hor_f*3600 - $hor_i*3600 | bc`
calcsegf=`echo $min_f*60 | bc`
calcsegf_t=`echo $calcsegf + $seg_f | bc`
calcsegi=`echo $min_i*60 | bc`
calcsegi_t=`echo $calcsegi + $seg_i | bc`
calc4=`echo $calcsegf_t - $calcsegi_t | bc`

#1 hora = 3600 segundos
#1 dia = 86400 segundos
#1 min = 60 segundos

seg_t=$(($calc1 + $calc2 + $calc3 + $calc4))

echo $seg_t