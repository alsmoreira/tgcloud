/*
 ============================================================================
 Name        : main.cpp
 Author      : Anderson Moreira
 E-mail		 : alsm4@cin.ufpe.br
 Version     : 08/11/2012
 Copyright   :
 Description : Cria arquivos de escalonamento do condor derivado do parent.dat
 	 	 	 	 e t.dat. Esses arquivos s�o gerados pelo start.sh
 ============================================================================
 */

#include "dag.h"

int main () {

	if (setHeaderScheduling() == 0)
	{
		cout << "COMPILE DAG FILE\n";
		setBodyScheduling();
		setMiddleScheduling();
		return 0;
	}
	else
	{
		cout << "\nERROR OPEN FILE";
		return 1;
	}
} // fim do main
