/*
 * dimacs.cpp
 *
 *  Created on: 26/12/2012
 *      Author: anderson
 *      Description: This project create the scheduling.dag file to use in HTC Condor
 *      for high-computing process. Other funcionality is generate file to set weight
 *      using DGT file.
 */

#include "dimacs.h"

char buffer[10000], buffer_out[10000];

vector<string> explode (char delimiter, string my_string) {
    char my_substring[my_string.size() + 1];
    vector<string> v;
    int size = 0;

    for (int i = 0; my_string[i] != '\0'; i++) {
        if (my_string[i] == delimiter && size > 0) {
            my_substring[size] = '\0';
            v.push_back(string(my_substring));
            size = 0;
        } else {
            my_substring[size++] = my_string[i];
        }
    }

    if (size > 0) {
        my_substring[size] = '\0';
        v.push_back(string(my_substring));
    }

    return v;
}

bool compare (string a, string b) {
    vector<string> va = explode(' ', a);
    vector<string> vb = explode(' ', b);

    for (int i = 1; i < va.size(); i++) {
        if (va[i] == "CHILD")
            return true;
        if (vb[i] == "CHILD")
            return false;

        int result = atoi(va[i].c_str()) - atoi(vb[i].c_str());

        if (result < 0)
            return true;
        else if (result > 0)
            return false;
    }
}

int dimacsJOB() {

	int failure = 0;
    char buffer[10000];

    freopen("./data/nodes.dat", "r", stdin);

    while (gets(buffer)) {
    	stringstream filename;
    	filename << "job.";
    	filename << buffer;

    	FILE *handle = fopen(filename.str().c_str(), "w");
    	if (handle == NULL)
    	{
    		failure = 1;
    	}

    	fprintf(handle,"Universe   = vanilla\n");
    	fprintf(handle,"Executable = simple\n");
    	fprintf(handle,"Requirements = (Name == \"vm1.intranet\") || (Name == \"vm2.intranet\") || (Name == \"vm3.intranet\") || (Name == \"vm4.intranet\") || (Name == \"vm5.intranet\")\n");
    	fprintf(handle,"Rank = ( (machine == \"vm1.intranet\") || (machine == \"vm2.intranet\") || (machine == \"vm3.intranet\") || (machine == \"vm4.intranet\") || (machine == \"vm5.intranet\") ) && memory\n");
    	fprintf(handle,"Arguments  = 2 10\n");
    	fprintf(handle,"Log        = log.simple.%s\n",filename.str().c_str());
    	fprintf(handle,"Output     = out.simple.%s\n",filename.str().c_str());
    	fprintf(handle,"Error      = err.simple.%s\n",filename.str().c_str());
    	fprintf(handle,"should_transfer_files   = YES\n");
    	fprintf(handle,"when_to_transfer_output = ON_EXIT\n");
    	fprintf(handle,"Queue");
    	fclose(handle);
    }
    return failure;
}

int setHeaderScheduling() {
	int failure = 0;
	// Cria arquivo scheduling.dag e verifica se o mesmo pode ser criado
	FILE *handle = fopen("./data/scheduling.dag", "w");
    if (handle == NULL)
    {
       	failure = 1;
    }

    // Abre arquivo nodes.dat, verifica quais n�o ter�o no task graph e escreve as premissas no scheduling.dag
    freopen("./data/nodes.dat", "r", stdin);
    while (gets(buffer)) {
    	stringstream filename;
    	filename << buffer;
    	fprintf(handle,"Job %s job.%s\n",buffer,filename.str().c_str());
    }
    fprintf(handle,"\n");

    freopen("./data/nodes.dat", "r", stdin);
    while (gets(buffer)) {
        	stringstream filename;
        	filename << buffer;
        	fprintf(handle,"SCRIPT PRE %s /opt/bench/scripts/pre.sh\n",buffer);
     }
     fprintf(handle,"\n");

     freopen("./data/nodes.dat", "r", stdin);
     while (gets(buffer)) {
             	stringstream filename;
             	filename << buffer;
             	fprintf(handle,"SCRIPT POST %s /opt/bench/scripts/post.sh\n",buffer);
     }
     fprintf(handle,"\n");
     fclose(handle);
     return failure;
}

int setBodyScheduling(){
	int failure = 0;

	map<string, vector<string> > children, parents;
	set<string> nodes;
	set<string>::iterator it;
	string node, child, garbage;
	string input_file = "./data/dimacs.dgt";
	string output_file = "./data/body.dag";

	fstream input(input_file.c_str(), fstream::in);
	fstream output(output_file.c_str(), fstream::out);

	input >> node;

	while (node != "EOF") {
		input >> garbage; // descarta o s�mbolo "->"
		input >> child;
		child = child.substr(0, child.size() - 1); // descarta o s�mbolo ";"
		children[node].push_back(child);
		parents[child].push_back(node);
		nodes.insert(child);
		nodes.insert(node);
		input >> node;
	}

	for (it = nodes.begin(); it != nodes.end(); it++) {
		if (parents[*it].size() > 1) {
			output << "PARENT ";

			for (int i = 0; i < parents[*it].size(); i++) {
				output << parents[*it][i] << " ";
				children[parents[*it][i]].erase(find(all(children[parents[*it][i]]), *it));
			}
			output << "CHILD " << *it << endl;
		}
	}

	for (it = nodes.begin(); it != nodes.end(); it++) {
		if (children[*it].size() > 0) {
			output << "PARENT " << *it << " CHILD ";

			for (int i = 0; i < children[*it].size(); i++)
            output << children[*it][i] << " ";
			output << endl;
		}
	}

	input.close();
	output.close();
	fstream input2(output_file.c_str(), fstream::in);

	vector<string> commands;
	string command;

	while (getline(input2, command))
		commands.push_back(command);

	sort(all(commands), compare);

	input2.close();
	fstream output2(output_file.c_str(), fstream::out);

	for (int i = 0; i < commands.size(); i++)
		output2 << commands[i] << endl;

	output2.close();

	return failure;
}

int joinScheduling() {
	int failure = 0;
	FILE *handle = fopen("./data/scheduling.dag", "a");
	freopen("./data/body.dag", "r", stdin);

	if (handle == NULL)
	{
	      	failure = 1;
	}

	while (gets(buffer_out)) {
		    	stringstream filename;
		    	filename << buffer_out;
		    	fprintf(handle,"%s \n",buffer_out);
    }

	fprintf(handle,"\n");
	fclose(handle);
	return failure;
}
