/*
 * dimacs.h
 *
 *  Created on: 26/12/2012
 *      Author: anderson
 */

#ifndef DIMACS_H_
#define DIMACS_H_


#include <sstream>
#include <stdio.h>
#include <cstdio>
#include <cstdlib>
#include <cstring>
#include <time.h>
#include <iostream>
#include <fstream>
#include <algorithm>
#include <utility>
#include <string>
#include <vector>
#include <list>
#include <map>
#include <set>

#define all(v) (v).begin(), (v).end()

using namespace std;

vector<string> explode (char delimiter, string my_string);
int setBodyScheduling();
int setHeaderScheduling();
int joinScheduling();
bool compare (string a, string b);
int dimacsJOB();


#endif /* DIMACS_H_ */
