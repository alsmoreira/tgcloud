/*
 * main.cpp
 *
 *  Created on: 19/12/2012
 *      Author: anderson
 *      Description: Main file for Dimacs DAG Project.
 */

#include "dimacs.h"

int main (int argv, char *argc[]) {

	if (setHeaderScheduling() == 0)
	{
		cout << "COMPILE DAG FILE\n";
		setBodyScheduling();
		joinScheduling();
		dimacsJOB();
		return 0;
	}
	else
	{
		cout << "\nERROR OPEN FILE";
		return 1;
	}
}



