/*
 * main.cpp
 *
 *  Created on: Nov 20, 2012
 *      Author: alsm
 *      Description: Projeto que cria o arquivo tgffoppt para geração de task graph
 */

#include <cstdio>
#include <cstdlib>
#include <iostream>
#include <sstream>
#include <string>
#include <stdio.h>
#include <stdlib.h>
#include <time.h>

using namespace std;

int main (int argc, char *argv[]) {

	int failure = 0;
	int input;

	if (argc != 2) {
		printf("Uso: starttgff <numero_task_graphs>\n");
	    failure = 1;
	}
	else {
		input = atoi(argv[1]);

		cout << "GENERATE TG FILE\n";

		FILE *handle = fopen("vm_task.tgffopt", "w");

		if (handle == NULL)
		{
			failure = 1;
		}

		fprintf(handle,"seed 1\n");
		fprintf(handle,"period_mul 1, 0.5, 2\n");
		fprintf(handle,"tg_cnt %d\n",input);
		fprintf(handle,"task_cnt 60 10\n");
		fprintf(handle,"gen_series_parallel\n");
		fprintf(handle,"series_wid 4 2\n");
		fprintf(handle,"series_len 5 3\n");
		fprintf(handle,"series_must_rejoin 0\n");
		fprintf(handle,"series_subgraph_fork_out .5\n");
		fprintf(handle,"tg_write \neps_write \nvcg_write\n");
		fprintf(handle,"table_label PACK \ntable_cnt 1\n");
		fprintf(handle,"table_attrib bound 80 20 \ntype_attrib exec_time 100 10 0.8 \ntrans_write\n");
		fclose(handle);

		return failure;
	}//fim do if
}//fim do main



