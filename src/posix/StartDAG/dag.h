/*
 * dag.h
 *
 *  Created on: 30/11/2012
 *      Author: anderson
 */

#ifndef DAG_H_
#define DAG_H_

#include <cstdio>
#include <cstdlib>
#include <cstring>
#include <iostream>
#include <sstream>
#include <fstream>
#include <algorithm>
#include <utility>
#include <string>
#include <vector>
#include <list>
#include <map>
#include <set>

using namespace std;

vector<string> explode (char delimiter, string my_string);
int compare_commands (string a, string b);
bool compare (string a, string b);
int setHeaderScheduling();
int setMiddleScheduling();
void setBodyScheduling();
int joinScheduling();

#endif /* DAG_H_ */
