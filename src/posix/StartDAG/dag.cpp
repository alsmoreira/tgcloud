/*
 * dag.cpp
 *
 *  Created on: 30/11/2012
 *      Author: anderson
 */

#include "dag.h"

#define all(v) (v).begin(), (v).end()

map<string, vector<string> > children, parents;
set<string> nodes;
set<string>::iterator it;
string node, child;

int failure = 0;
char buffer[100000], buffer_out[100000];

int setHeaderScheduling() {
	// Cria arquivo head.dag e verifica se o mesmo pode ser criado antigamente era o scheduling.dag
	FILE *handle = fopen("./data/head.dag", "w");

	if (handle == NULL)
	{
	   	failure = 1;
	}

	// Abre arquivo t.dat, verifica quais não terão no task graph e escreve as premissas no scheduling.dag
	freopen("./data/t.dat", "r", stdin);
	while (gets(buffer)) {
		stringstream filename;
		filename << buffer;
		fprintf(handle,"Job %s job.%s\n",buffer,filename.str().c_str());
	}
	fprintf(handle,"\n");
	fclose(handle);
	return failure;
}

int setMiddleScheduling(){

	FILE *handle = fopen("./data/middle.dag", "w");

	freopen("./data/t.dat", "r", stdin);
	while (gets(buffer)) {
	   	stringstream filename;
	   	filename << buffer;
	   	fprintf(handle,"SCRIPT PRE %s /opt/bench/scripts/pre.sh\n",buffer);
	}
	fprintf(handle,"\n");

	fclose(handle);
	return failure;
}

void setBodyScheduling(){
    string input_file = "./data/parent.dat";
    string output_file = "./data/body.dag";

    fstream input(input_file.c_str(), fstream::in);
    fstream output(output_file.c_str(), fstream::out);

    input >> node;

        while (node != "EOF") {
            input >> child;
            children[node].push_back(child);
            parents[child].push_back(node);
            nodes.insert(child);
            nodes.insert(node);
            input >> node;
        }

        for (it = nodes.begin(); it != nodes.end(); it++) {
            if (parents[*it].size() > 1) {
                output << "PARENT ";

                for (int i = 0; i < parents[*it].size(); i++) {
                    output << parents[*it][i] << " ";
                    children[parents[*it][i]].erase(find(all(children[parents[*it][i]]), *it));
                }
                output << "CHILD " << *it << endl;
            }
        }

        for (it = nodes.begin(); it != nodes.end(); it++) {
            if (children[*it].size() > 0) {
                output << "PARENT " << *it << " CHILD ";

                for (int i = 0; i < children[*it].size(); i++)
                    output << children[*it][i] << " ";
                output << endl;
            }
        }

        input.close();
        output.close();
        fstream input2(output_file.c_str(), fstream::in);

        vector<string> commands;
        string command;

        while (getline(input2, command))
            commands.push_back(command);

        sort(all(commands), compare);

        input2.close();
        fstream output2(output_file.c_str(), fstream::out);

        for (int i = 0; i < commands.size(); i++)
            output2 << commands[i] << endl;

        output2.close();
}

int joinScheduling() {
	FILE *handle = fopen("./data/scheduling.dag", "a");
	freopen("./data/body.dag", "r", stdin);

	if (handle == NULL)
	{
	      	failure = 1;
	}

	while (gets(buffer_out)) {
		    	stringstream filename;
		    	filename << buffer_out;
		    	fprintf(handle,"%s \n",buffer_out);
    }

	fprintf(handle,"\n");
	fclose(handle);
	return failure;
}

vector<string> explode (char delimiter, string my_string) {
    char my_substring[my_string.size() + 1];
    vector<string> v;
    int size = 0;

    for (int i = 0; my_string[i] != '\0'; i++) {
        if (my_string[i] == delimiter && size > 0) {
            my_substring[size] = '\0';
            v.push_back(string(my_substring));
            size = 0;
        } else {
            my_substring[size++] = my_string[i];
        }
    }

    if (size > 0) {
        my_substring[size] = '\0';
        v.push_back(string(my_substring));
    }

    return v;
}

int compare_commands (string a, string b) {
    vector<string> va = explode('_', a);
    vector<string> vb = explode('_', b);

    char n1[50];
    for (int i = 1; n1[i - 1] = '\0', i < va[0].size(); i++) n1[i - 1] = va[0][i];
    char n2[50];
    for (int i = 1; n2[i - 1] = '\0', i < vb[0].size(); i++) n2[i - 1] = vb[0][i];

    int n1a = atoi(n1);
    int n2a = atoi(va[1].c_str());
    int n1b = atoi(n2);
    int n2b = atoi(vb[1].c_str());

    if (n1a == n1b) {
        if (n2a < n2b) return -1;
        else if (n2a == n2b) return 0;
        else return 1;
    } else if (n1a < n1b) {
        return -1;
    } else {
        return 1;
    }
}

bool compare (string a, string b) {
    vector<string> va = explode(' ', a);
    vector<string> vb = explode(' ', b);

    for (int i = 1; i < va.size(); i++) {
        if (va[i] == "CHILD")
            return true;
        if (vb[i] == "CHILD")
            return false;

        int result = compare_commands(va[i], vb[i]);

        if (result < 0)
            return true;
        else if (result > 0)
            return false;
    }
}
