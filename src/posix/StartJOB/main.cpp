/*
 ============================================================================
 Name        : main.cpp
 Author      : Anderson Moreira
 E-mail		 : alsm4@cin.ufpe.br
 Version     : 06/11/2012
 Copyright   :
 Description : gera os jobs (nós) do condor com o valor interno de execução
 	 	 	 	 baseado no algoritmo Mersenne Twister.
 	 	 	 	 Executar em linha de comando na pasta Debug
 	 	 	 	 g++ ../main.cpp -o StartJOB ../randomaelf64.a
 	 	 	 	 Precisa do arquivo t.dat
 ============================================================================
 */

#include <cstdio>
#include <cstdlib>
#include <iostream>
#include <sstream>
#include <string>
#include <time.h> // Necessário  para gerar um número aleatório pelo time()
#include <stdio.h>
#include <randoma.h> //Inclui biblioteca do Mersenne Twister

using namespace std;

int main (int argc, char *argv[]) {

	int i, failure = 0;
	int seed = time(0);
	double roler;

    char buffer[100000];

    MersenneRandomInit(seed);

    cout << "GENERATE JOB FILES FOR CONDOR\n";

    freopen("./data/t.dat", "r", stdin);

    while (gets(buffer)) {
        stringstream filename;
        filename << "job.";
        filename << buffer;

        FILE *handle = fopen(filename.str().c_str(), "w");
        if (handle == NULL)
        {
        	failure = 1;
        }

        fprintf(handle,"Universe   = vanilla\n");
        fprintf(handle,"Executable = /opt/bench/bin/run\n");
        fprintf(handle,"Requirements = (Name == \"vm.intranet\") \n");
        fprintf(handle,"Rank = ( (machine == \"vm1.intranet\") || (machine == \"vm2.intranet\") || (machine == \"vm3.intranet\") || (machine == \"vm4.intranet\") || (machine == \"vm5.intranet\") ) && memory\n");
        for(i=0;i<2;i++)
        {
        	roler = MersenneIRandom(0,99); //era usado na linha abaixo
        }
        fprintf(handle,"Arguments  = $peso\n");
        fprintf(handle,"Log        = log/log.simple.%s\n",filename.str().c_str());
        fprintf(handle,"Output     = log/out.simple.%s\n",filename.str().c_str());
        fprintf(handle,"Error      = log/err.simple.%s\n",filename.str().c_str());
        fprintf(handle,"should_transfer_files   = YES\n");
        fprintf(handle,"when_to_transfer_output = ON_EXIT\n");
        fprintf(handle,"Queue");
        fclose(handle);
    }

    return failure;
}
