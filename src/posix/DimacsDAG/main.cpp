/*
 * main.cpp
 *
 *  Created on: 20/12/2012
 *      Author: anderson
 */

#include "dimacs.h"

int main (int argv, char *argc[]) {

	if (setHeaderScheduling() == 0)
	{
		cout << "COMPILE DAG FILE\n";
		setBodyScheduling();
		joinScheduling();
		dimacsJOB();
		return 0;
	}
	else
	{
		cout << "\nERROR OPEN FILE";
		return 1;
	}
}

