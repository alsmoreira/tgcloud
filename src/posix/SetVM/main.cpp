/*
 * main.cpp
 *
 *  Created on: 08/01/2013
 *      Author: anderson
 *      Description: sistema que verifica a vm mais livre nos jobs do condor
 */

#include <iostream>
#include <fstream>
#include <sstream>
#include <map>
#include <string>
#include <vector>

using namespace std;

vector<string> explode (char delimiter, string my_string);

int main (void) {
    int type;
    map<int, double> types;
    string line, job;

    stringstream buffer;
    ifstream taskTypeInput("/opt/bench/data/task_type.dat", ifstream::in);

    // substitui os valores nos jobs

    while ( getline(taskTypeInput, line) ) {
    	if (line != "EOF") {
            buffer << line;
            buffer >> job >> type;
            job = "job." + job;

            ifstream jobInput(job.c_str(), ifstream::in);

            if ( jobInput.good() ) {
                buffer.clear();
                ifstream vmInput("/opt/bench/data/order.dat"); //TO-DO: tem de colocar o script onevmmem.sh para jogar aqui o order.dat
                string vm;
                getline(vmInput, vm);
                vmInput.close();

                for (int i = 1; i <= 2; i++) {
                    getline(jobInput, line);
                    buffer << line << endl;
                }//fim do for

                getline(jobInput, line);
                buffer << "Requirements = (Name == \"" << vm << "\")" << endl;
                getline(jobInput, line);
                buffer << line << endl;
                buffer << "Arguments  = " << types[type] << endl;
                getline(jobInput, line);

                while ( getline(jobInput, line) ) {
                    buffer << line << endl;
                }

                jobInput.close();
                ofstream jobOutput(job.c_str(), ofstream::out);

                while ( getline(buffer, line) ) {
                    jobOutput << line << endl;
                }

                buffer.clear();
                jobOutput.close();
            }//fim do if

            buffer.clear();
        }//fim do if
    }//fim do while

    return 0;
}//fim do main

//changes
vector<string> explode (char delimiter, string my_string) {
    char my_substring[my_string.size() + 1];
    vector<string> v;
    int size = 0;

    for (int i = 0; my_string[i] != '\0'; i++) {
        if (my_string[i] == delimiter && size > 0) {
            my_substring[size] = '\0';
            v.push_back(string(my_substring));
            size = 0;
        } else {
            my_substring[size++] = my_string[i];
        }
    }

    if (size > 0) {
        my_substring[size] = '\0';
        v.push_back(string(my_substring));
    }

    return v;
}
