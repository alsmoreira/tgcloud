/*
 ============================================================================
 Name        : main.cpp
 Author      : Anderson Moreira
 E-mail		 : anderson.moreira@recife.ifpe.edu.br
 Version     : 14/11/2012
 Copyright   :
 Description :
 ============================================================================
 */


#include <cstdio>

#include <iostream>
#include <fstream>
#include <string>

using namespace std;

string trim (string my_string) {
    int begin = 0;
    int end = my_string.size() - 1;

    while (my_string[begin] == ' ') begin++;
    while (my_string[end] == ' ') end--;

    my_string = my_string.substr(begin, end + 1);
    return my_string;
}

string replace (string my_string, char old_char, char new_char) {
    for (int i = 0; i < my_string.size(); i++)
        if (my_string[i] == old_char)
            my_string[i] = new_char;
    return my_string;
}

int main (void) {
    string line, graph_name;

    freopen("vm_task.tgff", "r", stdin);

    while (getline(cin, line)) {
        if (line.find("@TASK_GRAPH") != string::npos) {
            graph_name = line.substr(1, line.find("{") - 1);
            graph_name = trim(graph_name);
            graph_name = replace(graph_name, ' ', '_');

            fstream file(graph_name.c_str(), fstream::out);

            while (line.find("}") == string::npos) {
                file << line << endl;
                getline(cin, line);
            }
            file << "}" << endl;
            file.close();
        }

        if (line.find("@PACK") != string::npos) {
            fstream file("PACKS.dat", fstream::out);
            file << line << endl;

            while (getline(cin, line)) {
                file << line << endl;
            }
            file.close();
        }
    }

    cout << "GENERATE PACK - DETACH FILES\n";
    return 0;
}

