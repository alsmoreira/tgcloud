/*
 ============================================================================
 Name        : main.cpp
 Author      : Anderson Moreira
 E-mail		 : alsm4@cin.ufpe.br
 Version     : 08/11/2012
 Copyright   :
 Description : sistema simples de execução temporária e cálculo matemático
 ============================================================================
 */

#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>

int main(int argc, char **argv)
{
    //float sleep_time;
    float input;
    int failure;

    if (argc != 2) {
        printf("Uso: run <float>\n");
        failure = 1;
    } else {
        //sleep_time = atof(argv[1]);
        input      = atof(argv[1]);

        //printf("Executando por %2.2f segundos...\n", sleep_time);
        //usleep(sleep_time*1000000);
        printf("Resultado x2: %.4f\n", input * 2);
        failure = 0;
    }
    return failure;
}
